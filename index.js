const serverless = require("serverless-http");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();

app.use(
  cors({
    exposedHeaders: ["Content-Length", "X-correlation-id"]
  })
);

app.use(bodyParser.json());

app.get("/", async function(req, res) {
  var now = Math.floor(new Date() / 1000);
  var content = "Welcome to home controller! This is Kubeless sample 02 - TIME = " + now;
  console.log(content);
  res.json({
    message: content
  });
  res.end();
});

app.post("/books", async function(req, res) {
  var now = Math.floor(new Date() / 1000);
  console.log("--> " + now);
  res.json({
    message: "You just hit POST books/ ! - TIME = " + now
  });
  res.end();
});

module.exports.handler = serverless(app);
